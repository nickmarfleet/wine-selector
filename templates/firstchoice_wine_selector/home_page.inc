<!--======================================================
		wine_selector
		home_page
	=======================================================-->

	
	<div class="home_page main">


			<div class="chalk_banner size_1 position_1 sprite transitionAll">
				<div class="rotate_1">
					<span class="light"><%= render :textual => 'chalk_banner_light_1', :default => 'weekly', :maxlength => '6' %></span>
					<span class="heavy"><%= render :textual => 'chalk_banner_heavy_1', :default => 'BEST BUYS', :maxlength => '6' %></span>
				</div>
			</div>

			

			<div class="sign_up_button blue_button type_1 transitionAll">
				<span class="light">sign me up</span>
				<span class="heavy">FOR DEALS</span>
				<div class="mail_icon sprite"></div>
				<div class="click_region"></div>
			</div>



			<!-- repeating region gallery -->
			<div data-stats="Main-Gallery-Scrolled" class="gallery_1">

				
				


				<%= render :navigation => "Feature-Wines" , :template_name => 'feature_wine_gallery.nav' %>
				




				<div class="gallery_nav_wrap">
					<div class="gallery_nav left sprite"></div>
					<div class="gallery_nav right sprite"></div>
					<!-- populate pagination with js -->
					<div class="inicator_area">
						<div class="indicator_bar"></div>
					</div>
				</div>
			</div> <!-- end gallery_1 -->







			<div class="matt position_1 sprite transitionAll"></div>

			<div class="click_area_for_wine_selector"></div>

			<div class="chalk_text position_1 transitionAll">
				<span class="line_1">Need a hand?</span>
				<span class="line_2">T<span class="kern_left">ry</span> this...</span>
			</div>

			<div class="green_button position_1 transitionAll">
				<span class="heavy">MATT'S</span>
				<span class="light">wine selector</span>
				<div class="bottle_icon sprite"></div>
			</div>

		


			
	</div> <!-- end main -->

		
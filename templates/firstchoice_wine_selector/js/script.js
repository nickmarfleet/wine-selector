// -------------------------- //
// WINDOW LOAD (ALL LOADED)
// -------------------------- //
$(window).load(function() {

	// ----------------------------------------
	// PRELOADER CLASS REMOVAL
	setTimeout(function(){
  		$('html').removeClass('preload');
  	},800);
  	// timeout should be a fraction longer than css transitions
  	// or other timing event using the preload class trigger.
  	// ----------------------------------------



});



// -------------------------- //
// MAIN APP
// -------------------------- //
$(function(){
	app.init();
});


var app = (function(){
	
	var _self = {};
	var reloadTimeoutRequired = false;
	var eventType = 'click';
		

	var selectors_group_1 = $('.chalk_banner.size_1.position_1, .sign_up_button, .chalk_back, .matt.position_1, .chalk_text.position_1, .green_button.position_1, .white_back, .fade, .click_area_for_wine_selector');

	var selectors_group_1b = $('.chalk_banner.size_1.position_1, .sign_up_button, .matt.position_1, .chalk_text.position_1, .click_area_for_wine_selector, .green_button.position_1, .gallery_1');

	var selectors_group_1c = $('.fade');

	var selectors_group_2 = $('.chalk_back, .white_back');

	var selectors_group_3 = $('.matt.position_2, .meet_matt_button');

	


	function init(){
		if ( Modernizr.touch ) {
			eventType = 'touchstart';
		}

		app.main_navigation.init();
		app.gallery_1.init();
		app.filter_gallery.init();
		app.price_layouts.init();
		app.date_scroller.init();
		app.form.init();
		app.form_nav.init();
		app.logging_stats.init();

	}


	function alterAddressForReload() {
		if ( reloadTimeoutRequired === false ) {
			window.location = ( window.location.href + '#' + eventType );
			reloadTimeoutRequired = true;
		}
	}


	_self = {
		init : init,
		alterAddressForReload: alterAddressForReload,
		selectors_group_1  : selectors_group_1,
		selectors_group_1b : selectors_group_1b,
		selectors_group_1c : selectors_group_1c,
		selectors_group_2  : selectors_group_2,
		selectors_group_3  : selectors_group_3
	};
	return _self;
})();


// ===================================================================================================================



























// -------------------------- //
// FORM
// -------------------------- //

app.form = (function(){

	var _self = {};
	var $form;
	var current_id;
	var eventType = 'click';

	function init(){
		if ( Modernizr.touch ) {
			eventType = 'touchstart';
		}

		//Form Validation
		$form = $("#standard_form");
		var fa = $form.find('.form_action').text();
		$form.attr('action', fa);
		updateDOBfield();
		tabToNextCapture();
		fauxCheckboxes();
		fauxSelects();

		$form.find(":input").focus(function () {
		     current_id = this.id;
		});

		if($form.length){
			$form.validate({
				/*
				submitHandler: function(form) {
			     $(form).ajaxSubmit();
			    },
			    */

				// Change the default error element to <em> for easy hiding with CSS if not required
				errorElement: "em",
				
				//Adds "error" class to input label
				highlight: function(element, errorClass, validClass) {
					toggleHighlight(element,errorClass,validClass,false);
				},
			
				//Removes "error" class to input label
				unhighlight: function(element, errorClass, validClass) {
					toggleHighlight(element,errorClass,validClass,true);
				},

				errorPlacement: function(error, element) {
					if(element.attr('type')=='checkbox'){
						error.appendTo( element.closest('fieldset') );
					}else if(element.type=='select'){
						error.appendTo(element.parent());
					}else{
						error.appendTo(element.parent());
					}
				}

			});
		}

		$form.on('submit', function(e) {
            e.preventDefault(); // <-- important
            if ( $form.find(':input.error').length < 1 ) {
            	
        //      	if (typeof forward === "undefined"){
	              $(this).ajaxSubmit({
	              	dataType: 'jsonp',
	              	clearForm: true,
	              	beforeSubmit: function() {
	         		    },	
	                success: formSuccess
	              });
         //    }else{
         //       forward.send('POST', fa, $form.serialize(), '', '');
         //       formSuccess();
	       //   }
        }
      });

  } // end init

  function formSuccess(){
	  var thanksDiv = '<div id="thanksDiv"><span>Thanks for signing up!</span></div>';
	 	$('body').append(thanksDiv);
	 	app.logging_stats.log('signup', $form, '');
	  setTimeout( function(){
	    $('#postcode').trigger('blur');
		  $('.form_div .close_form').trigger(eventType);
		}, 0 );

		setTimeout( function(){
		  $('#thanksDiv').remove();
		}, 3700 );
  }

	function tabToNextCapture() {
		$form.on('keydown', function(e) { 

			var code = (e.keyCode ? e.keyCode : e.which);
			if(code === 13) { //Enter keycode
			   //Do something
			}
			if(code === 9) { //Tab keycode
			   	e.preventDefault();
			   	if ( current_id === 'first_name' ) {
			   		$('#email').focus();
			   	} else if ( current_id === 'email' ) {
			   		$('#postcode').focus();
			   	}
				
			   		
			}
		});
	}




	function fauxSelects(){
		$('select.custom').not('.applied').each(function(i){
			$(this).addClass('applied').customStyle(200,20,('select_num_' + i));
		});

		$('#dob_day, #dob_month, #dob_year').on('change', function(){
			updateDOBfield();
			$('#postcode').focus();
		});
	}

	function updateDOBfield() {
		var newValDay = $('#dob_day').val();
		var newValMonth = $('#dob_month').val();
		var newValYear = $('#dob_year').val();
		var dobString = (newValDay +'-'+ newValMonth +'-'+ newValYear);
		
		$('#dob').val(dobString);
	}



	function fauxCheckboxes(){
		// $('.standard_form input[type="checkbox"]').each(function(i){

		// 	var c = $(this);
		// 	var cp = c.parent();
			
		// 	cp.click(function(){
		// 		if ( c.is(':checked') ) {
		// 			c.removeAttr('checked');
		// 			cp.removeClass('active');
		// 		} else {
		// 			c.attr('checked', true);
		// 			cp.addClass('active');
		// 		}
		// 	});
		// });
	}


	function toggleHighlight(element,errorClass,validClass,isValid){
		
		var type = $(element).attr('type');
		
		var classToAdd = isValid?validClass:errorClass;
		var classToRemove = isValid?errorClass:validClass;
		
		if(type === "checkbox"){
			$(element).closest("fieldset").find(".ez-checkbox").removeClass(classToRemove).addClass(classToAdd);
			$(element).closest("fieldset").find("label").removeClass(classToRemove).addClass(classToAdd);
			return;
		}
		
		if(type === "radio"){
			$(element).closest("fieldset").find(".ez-radio").removeClass(classToRemove).addClass(classToAdd);
			$(element).closest("fieldset").find("label").removeClass(classToRemove).addClass(classToAdd);
			return;
		}
		
		$(element).removeClass(classToRemove).addClass(classToAdd);
		$(element.form).find("label[for=" + element.id + "]").removeClass(classToRemove).addClass(classToAdd);
	}





	_self = {
		init            : init,
    eventType       : eventType
	};
	
	return _self;
	
})();













// ----------------------------------------------------- //
// FORM NAV
// ----------------------------------------------------- //

app.form_nav = (function(){
	
	var _self = {};

	var formDiv = $('.form_div').eq(0);
	var eventType = 'click';
	var eventType2 = 'mousedown';
	var actionTarget;


	function init(){
		if ( Modernizr.touch ) {
			eventType = 'touchstart';
			eventType2 = 'touchstart';
		}
		bindEvents();
	} // end init



	function bindEvents() {

		$('body').on(eventType2, function(event){
			actionTarget = event.target;
		});


		formDiv.on(eventType, '.close_form', function(){
			var fp = $('.sign_up_form_page');
			fp.find('input[type="text"]').val('');
			$('#dob_day').val('1').trigger('change');
			$('#dob_month').val('JAN').trigger('change');
			$('#dob_year').val('1970').trigger('change');

			formDiv.find('.in_focus').removeClass('in_focus');
			formDiv.removeClass('field_focus');
			$('.sign_up_form_page .chalk_banner').show();

			fp.removeClass('show');

		});

		formDiv.on(eventType, function(){

		});
		
		formDiv.on('focus', 'input[type="text"], input[type="email"]', function(){
			var _this = $(this);
			var myParent = _this.parent();
			myParent.addClass('in_focus');
			formDiv.addClass('field_focus');
			$('.sign_up_form_page .chalk_banner').hide();
			setTimeout(function(){
				if( ! _this.is(':focus') ) {
					actionTarget.focus();
				}
			},100);

		});

		formDiv.on('blur', 'input[type="text"], input[type="email"]', function(e){
			if (   ( ! $(actionTarget).hasClass('customStyleSelectBox') )  &&  ( ! $(actionTarget).hasClass('customStyleSelectBoxInner') )  &&  ( ! $(actionTarget).hasClass('select_ui_element') )  &&  ( $(actionTarget).attr('id') !== 'submit_enquiry' )&&  ( ! $(actionTarget).hasClass('close_form') )   ) {
				
				var _this = $(this);
				var myParent = _this.parent();
				myParent.removeClass('in_focus');
				formDiv.removeClass('field_focus');
				$('.sign_up_form_page .chalk_banner').show();
			}
		});


		formDiv.on('input', '#email_part_1, #email_part_2', function(){
			var p1 = $('#email_part_1').val();
			var p2 = $('#email_part_2').val();
			var theEmail = (p1 + '@' + p2);
			$('#email').val(theEmail);

		});


		$('.customStyleSelectBox.select_num_0').on(eventType, function(){
			$('#dayPicker').show();
				app.date_scroller.refreshDayScroller();
		});

		$('#dayPicker').on('click', '.day', function(){
			var _this = $(this);
			$('#dayPicker .day.selected').not(_this).removeClass('selected');
			_this.addClass('selected');
		});

		$('.customStyleSelectBox.select_num_1').on(eventType, function(){
			$('#monthPicker').show();
			app.date_scroller.refreshMonthScroller();
		});

		$('#monthPicker').on('click', '.month', function(){
			var _this = $(this);
			$('#monthPicker .month.selected').not(_this).removeClass('selected');
			_this.addClass('selected');
		});

		$('.customStyleSelectBox.select_num_2').on(eventType, function(){
			$('#yearPicker').show();
			app.date_scroller.refreshYearScroller();
		});

		$('#yearPicker').on('click', '.year', function(){
			var _this = $(this);
			$('#yearPicker .year.selected').not(_this).removeClass('selected');
			_this.addClass('selected');
		});

		$('#dayPicker .set').on(eventType, function(){
			var selected = $('#dayPicker .day.selected');
			var selected_value = '1';
			if ( selected.length > 0 ) {
				selected_value = selected.eq(0).find('span').html();
			}
			$('.customStyleSelectBox.select_num_0 .customStyleSelectBoxInner').html(selected_value);
			$('#dayPicker').hide();
			updateDOBfield();
		});

		$('#dayPicker .cancel').on(eventType, function(){
			$('#dayPicker').hide();
		});


		$('#monthPicker .set').on(eventType, function(){
			var selected = $('#monthPicker .month.selected');
			var selected_value = 'JAN';
			if ( selected.length > 0 ) {
				selected_value = selected.eq(0).find('span').html();
			}
			$('.customStyleSelectBox.select_num_1 .customStyleSelectBoxInner').html(selected_value);
			$('#monthPicker').hide();
			updateDOBfield();
		});

		$('#monthPicker .cancel').on(eventType, function(){
			$('#monthPicker').hide();
		});

		
		$('#yearPicker .set').on(eventType, function(){
			var selected = $('#yearPicker .year.selected');
			var selected_value = '1990';
			if ( selected.length > 0 ) {
				selected_value = selected.eq(0).find('span').html();
			}
			$('.customStyleSelectBox.select_num_2 .customStyleSelectBoxInner').html(selected_value);
			$('#yearPicker').hide();
			updateDOBfield();
		});

		$('#yearPicker .cancel').on(eventType, function(){
			$('#yearPicker').hide();
		});

		
	}



	function updateDOBfield() {
		var newValDay = $('.customStyleSelectBox.select_num_0 .customStyleSelectBoxInner').html();
		var newValMonth = $('.customStyleSelectBox.select_num_1 .customStyleSelectBoxInner').html();
		var newValYear = $('.customStyleSelectBox.select_num_2 .customStyleSelectBoxInner').html();
		var dobString = (newValDay +'-'+ newValMonth +'-'+ newValYear);
		
		$('#dob').val(dobString);
	}

	
	
	_self = {
		init : init
	};
	return _self;
})();












// ----------------------------------------------------- //
// CUSTOM SCROLL-SELECTOR UI COMPONENT
// ----------------------------------------------------- //

app.date_scroller = (function(){
	
	var _self = {};
	var dayScroller;
	var monthScroller;
	var yearScroller;


	function init(){
		setupScrollers();
	} // end init



	function setupScrollers() {
	

		var now = new Date();
		var startYear = now.getFullYear() - 100;
        var endYear = now.getFullYear() - 18;

        // ====================== DAYS ==============================
		dayScroller = new iScroll( $('#dayPickeriScroll')[0], {
			snap: '.day',
			momentum: true,
			hScrollbar: false
			//vScrollbar: false 
		});


		// ====================== MONTHS =============================
		monthScroller = new iScroll( $('#monthPickeriScroll')[0], {
			snap: '.month',
			momentum: true,
			hScrollbar: false
			//vScrollbar: false 
		});


		// ====================== YEARS ==============================
		var newYears = '';
		for (i=startYear; i<(endYear+1); i++ ) {
			newYears += '<div class="year"><span>'+i+'</span></div>';
		}

		$('#yearScroller').append(newYears);
		
		yearScroller = new iScroll( $('#yearPickeriScroll')[0], {
			snap: '.year',
			momentum: true,
			hScrollbar: false
			//vScrollbar: false 
		});

	
	}


	function refreshDayScroller() {
		dayScroller.refresh();
	}
	function refreshMonthScroller() {
		monthScroller.refresh();
	}
	function refreshYearScroller() {
		yearScroller.refresh();
		yearScroller.scrollTo(0, -3180, 200);
	}



	
	
	_self = {
		init : init,
		refreshDayScroller : refreshDayScroller,
		refreshMonthScroller : refreshMonthScroller,
		refreshYearScroller : refreshYearScroller
	};
	return _self;
})();





















// ----------------------------------------------------- //
// MAIN NAVIGATION
// ----------------------------------------------------- //

app.main_navigation = (function(){
	
	var _self = {};

	var appendRegion = $('#append_content');
	var eventType = 'click';



	function init(){
		if ( Modernizr.touch ) {
			eventType = 'touchstart';
		}
		bindEvents();
	} // end init




	function bindEvents() {
		// launching the wine selector
		$('#chalk_back, .matt.position_1, .chalk_text.position_1, .click_area_for_wine_selector').on(eventType, function(){
			var button = $('.home_page .green_button.position_1');
			app.feedback.go(button);
			button.trigger(eventType);
		});

		$('.home_page .green_button.position_1').on(eventType, function(){
			app.selectors_group_1b.addClass('out_1');
			app.selectors_group_2.removeClass('position_1').addClass('position_2');
			$('.wine_selection_page_1').addClass('show');
			app.alterAddressForReload();
			app.logging_stats.log( 'pages', $('.wine_selection_page_1'), '' );
		});

		// Back button on wine selector
		$('.back_from_wine_selection_page_1').on(eventType, function(){
			app.selectors_group_1b.removeClass('out_1');
			app.selectors_group_2.removeClass('position_2').addClass('position_1');
			$('.wine_selection_page_1').removeClass('show');
			// reset the wine selector
			$('.section_1 .selector_button .close_icon').trigger(eventType);
		});

		// RED
		$('.selector_button.choice_red .click_region').on(eventType, function(){
			$(this).parent().addClass('current');
			$('.selector_button.choice_white').addClass('hide');
			$('.selector_button.choice_sparkling_champagne').addClass('hide');
			$('.selection_area .section_2').addClass('show');
			
			//check if there are any in each price range
			var inCategory = $('.gallery_slide_content .filter_red');
			$('.section_2 .selector_button').removeClass('NA');

			if (  inCategory.filter('.filter_10_20').length < 1  ) {
				$('.selector_button.choice_10_20 ').addClass('NA');
			}
			if (  inCategory.filter('.filter_20_30').length < 1  ) {
				$('.selector_button.choice_20_30 ').addClass('NA');
			}
			if (  inCategory.filter('.filter_30_50').length < 1  ) {
				$('.selector_button.choice_30_50 ').addClass('NA');
			}
			if (  inCategory.filter('.filter_50_100').length < 1  ) {
				$('.selector_button.choice_50_100 ').addClass('NA');
			}
			app.logging_stats.log( 'wine-selector', $('.selector_button.choice_red'), '' );
			_gaq.push(['_trackEvent', 'Matt\'s Selector', 'Wine Type Selection', 'Red Wine']);
		});

		$('.selector_button.choice_red .close_icon').on(eventType, function(){
			$(this).parent().removeClass('current');
			$('.selector_button.choice_white').removeClass('hide');
			$('.selector_button.choice_sparkling_champagne').removeClass('hide');
			$('.selection_area .section_2').removeClass('show');
			$('.section_2 .selector_button .close_icon').trigger(eventType);
			//remove any NA styles
			$('.section_2 .selector_button').removeClass('NA');
		});

		// WHITE
		$('.selector_button.choice_white .click_region').on(eventType, function(){
			$(this).parent().addClass('current');
			$('.selector_button.choice_red').addClass('hide');
			$('.selector_button.choice_sparkling_champagne').addClass('hide');
			$('.selection_area .section_2').addClass('show');

			//check if there are any in each price range
			var inCategory = $('.gallery_slide_content .filter_white');
			$('.section_2 .selector_button').removeClass('NA');

			if (  inCategory.filter('.filter_10_20').length < 1  ) {
				$('.selector_button.choice_10_20 ').addClass('NA');
			}
			if (  inCategory.filter('.filter_20_30').length < 1  ) {
				$('.selector_button.choice_20_30 ').addClass('NA');
			}
			if (  inCategory.filter('.filter_30_50').length < 1  ) {
				$('.selector_button.choice_30_50 ').addClass('NA');
			}
			if (  inCategory.filter('.filter_50_100').length < 1  ) {
				$('.selector_button.choice_50_100 ').addClass('NA');
			}
			app.logging_stats.log( 'wine-selector', $('.selector_button.choice_white'), '' );
			_gaq.push(['_trackEvent', 'Matt\'s Selector', 'Wine Type Selection', 'White Wine']);
		});

		$('.selector_button.choice_white .close_icon').on(eventType, function(){
			$(this).parent().removeClass('current');
			$('.selector_button.choice_red').removeClass('hide');
			$('.selector_button.choice_sparkling_champagne').removeClass('hide');
			$('.selection_area .section_2').removeClass('show');
			$('.section_2 .selector_button .close_icon').trigger(eventType);
			//remove any NA styles
			$('.section_2 .selector_button').removeClass('NA');
		});

		// SPARKLING / CHAMPAGNE
		$('.selector_button.choice_sparkling_champagne .click_region').on(eventType, function(){
			$(this).parent().addClass('current');
			$('.selector_button.choice_red').addClass('hide');
			$('.selector_button.choice_white').addClass('hide');
			$('.selection_area .section_2').addClass('show');

			//check if there are any in each price range
			var inCategory = $('.gallery_slide_content .filter_sparkling_champagne');
			$('.section_2 .selector_button').removeClass('NA');

			if (  inCategory.filter('.filter_10_20').length < 1  ) {
				$('.selector_button.choice_10_20 ').addClass('NA');
			}
			if (  inCategory.filter('.filter_20_30').length < 1  ) {
				$('.selector_button.choice_20_30 ').addClass('NA');
			}
			if (  inCategory.filter('.filter_30_50').length < 1  ) {
				$('.selector_button.choice_30_50 ').addClass('NA');
			}
			if (  inCategory.filter('.filter_50_100').length < 1  ) {
				$('.selector_button.choice_50_100 ').addClass('NA');
			}
			app.logging_stats.log( 'wine-selector', $('.selector_button.choice_sparkling_champagne'), '' );
			_gaq.push(['_trackEvent', 'Matt\'s Selector', 'Wine Type Selection', 'Sparkling / Champagne']);
		});

		$('.selector_button.choice_sparkling_champagne .close_icon').on(eventType, function(){
			$(this).parent().removeClass('current');
			$('.selector_button.choice_red').removeClass('hide');
			$('.selector_button.choice_white').removeClass('hide');
			$('.selection_area .section_2').removeClass('show');
			$('.section_2 .selector_button .close_icon').trigger(eventType);
			//remove any NA styles
			$('.section_2 .selector_button').removeClass('NA');
		});


		// BASIC PRICE range functions
		$('.section_2 .selector_button .click_region').on(eventType, function(){
			var myParent = $(this).parent();
			if ( ! myParent.hasClass('NA') ) {
				myParent.addClass('current');
				$('.section_1 .selector_button.current').addClass('position_2');
				$('.section_2 .selector_button').not(myParent).addClass('hide');
				$('.chalk.intro_2').addClass('hide');
				$('.chalk.intro_1').html('Selection').addClass('position_2');
				app.selectors_group_3.addClass('out_3');
				$('.wine_filter_gallery').addClass('show');
				app.logging_stats.log( 'wine-selector', $('.section_1 .selector_button.current'), myParent );
				var price_range_string_for_event_tracking = myParent.attr('data-stats');
				_gaq.push(['_trackEvent', 'Matt\'s Selector', 'Price Selection', price_range_string_for_event_tracking]);
			}
		});

		$('.section_2 .selector_button .close_icon').on(eventType, function(){
			var myParent = $(this).parent();
			myParent.removeClass('current');
			$('.section_1 .selector_button').removeClass('position_2');
			$('.section_2 .selector_button').not(myParent).removeClass('hide');
			$('.chalk.intro_2').removeClass('hide');
			$('.chalk.intro_1').html('1. Select').removeClass('position_2');
			app.selectors_group_3.removeClass('out_3');
			$('.wine_filter_gallery').removeClass('show');
		});


		// GALLERY FILTERS 
		$('.section_1 .selector_button .click_region').on(eventType, function(){
			if (  $('.section_2 .selector_button.current').length < 1  ) {
				var allFilters = 'filter_red filter_white filter_sparkling_champagne ' +
								 'filter_10_20 filter_20_30 filter_30_50 filter_50_100';
				var filterGallery = $('.wine_filter_gallery');
				filterGallery.removeClass(allFilters);
				var _this = $(this).parent();
				if ( _this.hasClass('choice_red') ) {
					filterGallery.addClass('filter_red');
				}
				if ( _this.hasClass('choice_white') ) {
					filterGallery.addClass('filter_white');
				}
				if ( _this.hasClass('choice_sparkling_champagne') ) {
					filterGallery.addClass('filter_sparkling_champagne');
				}
			}
		});

		$('.section_2 .selector_button .click_region').on(eventType, function(){
			if ( ! $(this).parent().hasClass('NA') ) {
				var priceFilters = 'filter_10_20 filter_20_30 filter_30_50 filter_50_100';
				var filterGallery = $('.wine_filter_gallery');
				filterGallery.removeClass(priceFilters);
				var _this = $(this).parent();
				if ( _this.hasClass('choice_10_20') ) {
					filterGallery.addClass('filter_10_20');
				}
				if ( _this.hasClass('choice_20_30') ) {
					filterGallery.addClass('filter_20_30');
				}
				if ( _this.hasClass('choice_30_50') ) {
					filterGallery.addClass('filter_30_50');
				}
				if ( _this.hasClass('choice_50_100') ) {
					filterGallery.addClass('filter_50_100');
				}
				app.filter_gallery.update();
			}
		});



		// SIGN UP FORM LAUNCH
		$('.sign_up_button .click_region').on(eventType, function(){
			$('.sign_up_form_page').addClass('show');
			app.alterAddressForReload();
			app.logging_stats.log( 'pages', $('.sign_up_form_page'), '' );
		});

		// MEET MATT PAGE LAUNCH
		$('.meet_matt_button').on(eventType, function(){
			$('.meet_matt_page').addClass('show');
			app.logging_stats.log( 'pages', $('.meet_matt_page'), '' );
		});

		$('.meet_matt_page').on(eventType, '.back_from_meet_matt_page', function(){
			$('.meet_matt_page').removeClass('show');
		});
		
		
	}


	
	
	_self = {
		init : init
	};
	return _self;
})();












// ----------------------------------------------------- //
// A FAKE HOVER ACTIVE FEEDBACK 
// ----------------------------------------------------- //

app.feedback = (function(){
	
	var _self = {};


	function init(){
	
	} // end init



	function feedback(element) {
		
		element.addClass('active');
		setTimeout( function(){
			element.removeClass('active');
		}, 200 );
		
		
	}


	
	
	_self = {
		init : init,
		go : feedback
	};
	return _self;
})();
















// ----------------------------------------------------- //
// PRICE LAYOUTS
// ----------------------------------------------------- //

app.price_layouts = (function(){
	
	var _self = {};


	function init(){
		addClassForThreeFigures();
		improveKerningOnPrices();
	} // end init



	function addClassForThreeFigures() {
		
		var allPrices = $('.price .dollar_value, .wine_filter_gallery .deal .dollar_value');
		allPrices.each(function(i){
			var _this = $(this);
			var myHTML = _this.html();
			if ( myHTML.length > 2 ) {
				_this.parent().addClass('three_figures');
			}
		});
		
	}


	function improveKerningOnPrices() {
		$('.dollar_value, .cents_value').each(function(i){
			var _this = $(this);
			var myText = _this.html();
			var position_of_1 = myText.indexOf('1');
			var newHTML = '';
			if ( position_of_1 !== -1 ) {
				var chars = myText.split('');
				chars_len = chars.length;
				var double_1 = 0;
				for ( i=0; i<chars_len; i++ ) {
					var currentChar = chars[i];
					if ( currentChar === '1' ) {
						newHTML += '<span class="kern_1">' + currentChar + '</span>';
						double_1 += 1;
					} else {
						newHTML += currentChar;
					}
				}
				_this.html(newHTML);
				if ( double_1 > 1 ) {
					_this.addClass('double_1');
				}
			} else {
				// newHTML = myText;
			}
		});

	}


	
	
	_self = {
		init : init
	};
	return _self;
})();

















// ----------------------------------------------------- //
// MAIN IMAGE SLIDER GALLERY
// ----------------------------------------------------- //

app.gallery_1 = (function(){
	
	var _self = {};

	var gallery = $('.gallery_1');
	var everyItem = gallery.find('.gallery_item');
	var everyItemLength = everyItem.length;
	var indi_bar = gallery.find('.indicator_bar');

	// var safe_click = true;
	var slide_time = 500;
	var eventType = 'click';
	var autoAnimate = true;
	var autoSlideMilliSeconds = 10000;
	var delayStartTimeout;



	function init(){
		if ( Modernizr.touch ) {
			eventType = 'touchstart';
		}

		fixMoreButtonPosition();

		if ( everyItemLength > 1 ) {
			setupItemIndicator();
			bindEvents();
			revealGalleryNav();
			startAutomation(autoSlideMilliSeconds);
		} else {

		}
	} // end init


	function startAutomation(interval) {
		autoScroll(interval);
	}

	function delayStartAutomation(interval) {
		if ( delayStartTimeout ) {
			clearTimeout(delayStartTimeout);
		}
		delayStartTimeout = setTimeout(function(){
			autoAnimate = true;
			startAutomation(autoSlideMilliSeconds);
		}, 90000);
	}

	function autoScroll(interval) {
		setTimeout(function(){
			if (autoAnimate === true) {
				goRight();
				autoScroll(interval);
			}
		},interval);
	}


	function revealGalleryNav() {
		gallery.find('.gallery_nav_wrap').fadeIn(500);
	}


	function bindEvents() {
		
		gallery.on(eventType, '.gallery_nav.left', function(){
			goLeft();
			autoAnimate = false;
			delayStartAutomation();
		});

		gallery.on(eventType, '.gallery_nav.right', function(e){
			goRight();
			autoAnimate = false;
			delayStartAutomation();
		});

		indi_bar.on(eventType, '.indicator', function(){
			goTo( $(this).index() );
			autoAnimate = false;
			delayStartAutomation();
		});

		everyItem.on(eventType, function(){
			var page_link = $(this).find('.link_to_wine_page').html();

			gallery.addClass('out_1');
			app.selectors_group_1.addClass('out_1');
			setTimeout(function(){
				$( ('.wine_page_' + page_link) ).addClass('show from_home_page');
			},700);

			app.alterAddressForReload();
			app.logging_stats.log( 'pages', $( ('.wine_page_' + page_link) ), '' );
		});

		$('.append_content').on(eventType, '.wine_page.from_home_page .back_from_wine_page', function() {
			$(this).parent().removeClass('show from_home_page');
			setTimeout(function(){
				app.selectors_group_1.removeClass('out_1');
				gallery.removeClass('out_1');
			},400);
		});


		 

	}

	function fixMoreButtonPosition() {
		// if the heading pushes the more button too far down then fix it
		var allButtons = gallery.find('.more_about');
		allButtons.each(function(i){
			var _this = $(this);
			// average should be 363px
			if ( _this.offset().top > 370 ) {
				_this.css({
					'left' : '235px',
					'top'  : '114px'
				})
			}
		});
	}


	function setupItemIndicator() {
		
		var indicatorHTML = '';
		for ( i=0; i<everyItemLength; i++) {
			if (i === 0 ) {
				indicatorHTML += '<div class="indicator sprite current"></div>';
			} else {
				indicatorHTML += '<div class="indicator sprite"></div>';
			}
		}
		indi_bar.append( indicatorHTML );
	}



	function goRight() {
		var delayIfNeeded = 0;
		var currentItem = everyItem.filter('.current');
		var current_index = currentItem.index();
		if ( (current_index + 1) >= everyItemLength ) {
			var nextOne = 0;
			delayIfNeeded = 100;
			everyItem.not(currentItem).removeClass('slide left').addClass('right');
		} else {
			var nextOne = (current_index + 1);
		}

		var nextItem = everyItem.eq(nextOne);

		if ( nextItem.hasClass('left') ) {
			delayIfNeeded = 100;
			everyItem.not(currentItem).removeClass('slide left').addClass('right');
			nextOne = 0; // we have used this var so ok to use as switch
		}
	
		setTimeout(function(){
  
			nextItem.addClass('current slide').removeClass('right');
			currentItem.removeClass('current').addClass('left slide');

			if ( nextOne === 0 ) {
				setTimeout(function(){
					currentItem.removeClass('slide left').addClass('right');
				},( delayIfNeeded * 4 ) );
			}

			indi_bar.find('.indicator').removeClass('current');
			indi_bar.find('.indicator').eq(nextOne).addClass('current');

		},delayIfNeeded);
	}



	function goLeft() {
		var delayIfNeeded = 0;
		var currentItem = everyItem.filter('.current');
		var current_index = currentItem.index();
		if ( (current_index - 1) < 0 ) {
			var nextOne = (everyItemLength - 1);
			delayIfNeeded = 100;
			everyItem.not(currentItem).removeClass('slide right').addClass('left');
		} else {
			var nextOne = (current_index - 1);
		}

		var nextItem = everyItem.eq(nextOne);
	
		if ( nextItem.hasClass('right') ) {
			delayIfNeeded = 100;
			everyItem.not(currentItem).removeClass('slide right').addClass('left');
			nextOne = (everyItemLength - 1); // we have used this var so ok to use as switch
		}

		setTimeout(function(){
  		
			nextItem.addClass('current slide').removeClass('left');
			currentItem.removeClass('current').addClass('right slide');

			if ( nextOne === (everyItemLength - 1) ) {
				setTimeout(function(){
					currentItem.removeClass('slide right').addClass('left');
				},( delayIfNeeded * 4 ) );
			}

			indi_bar.find('.indicator').removeClass('current');
			indi_bar.find('.indicator').eq(nextOne).addClass('current');

		},delayIfNeeded);
	}



	function goTo(nextOne) {
		var delayIfNeeded = 0;
		var currentItem = everyItem.filter('.current');
		var current_index = currentItem.index();
		if (nextOne === current_index) {
			return;
			// do nothing we are already there
		}
		

		if ( nextOne < current_index ) {
			delayIfNeeded = 100;
			everyItem.not(currentItem).removeClass('slide right').addClass('left');
			var direction = 'right';
		} else {
			delayIfNeeded = 100;
			everyItem.not(currentItem).removeClass('slide left').addClass('right');
			var direction = 'left';
		}

		var nextItem = everyItem.eq(nextOne);
	
		setTimeout(function(){
  		
  			if (direction === 'right') {

				nextItem.addClass('current slide').removeClass('left');
				currentItem.removeClass('current').addClass('right slide');

				setTimeout(function(){
					currentItem.removeClass('slide left').addClass('right');
				},( delayIfNeeded * 4 ) );
				

			} else {

				nextItem.addClass('current slide').removeClass('right');
				currentItem.removeClass('current').addClass('left slide');

				setTimeout(function(){
					currentItem.removeClass('slide right').addClass('left');
				},( delayIfNeeded * 4 ) );
			}

			indi_bar.find('.indicator').removeClass('current');
			indi_bar.find('.indicator').eq(nextOne).addClass('current');

		},delayIfNeeded);
	}


	
	_self = {
		init : init
	};
	return _self;
})();














// ----------------------------------------------------- //
// FILTER WINES IMAGE SLIDER GALLERY
// ----------------------------------------------------- //

app.filter_gallery = (function(){
	
	var _self = {};

	var gallery = $('.wine_filter_gallery');
	var slideContent = gallery.find('.gallery_slide_content');
	var everyItem = gallery.find('.panel');
	
	var indi_bar = gallery.find('.indicator_bar');

	var eventType = 'click';
	var autoAnimate = false;
	var autoSlideMilliSeconds = 10000;
	var pages = 1;
	var currentLocation = 0;
	var nowVisibleLength = 0;



	function init(){
		if ( Modernizr.touch ) {
			eventType = 'touchstart';
		}
		update();
		revealGalleryNav();
		bindEvents();
		setupItemIndicator();
		// startAutomation(autoSlideMilliSeconds);
	} // end init


	function update() {
		currentLocation = 0;
		calculatePages();
		setupItemIndicator();
		centerIfOnly2orLess();
		calculateDirectionButtons();
	}

	function calculateDirectionButtons() {
		if (currentLocation < 1) {
			gallery.find('.gallery_nav.left').hide();
		} else {
			gallery.find('.gallery_nav.left').show();
		}
		
		if (  (pages < 2) || ( (currentLocation + 1) === pages )  ) {
			gallery.find('.gallery_nav.right').hide();
		} else {
			gallery.find('.gallery_nav.right').show();
		}
	}

	function startAutomation(interval) {
		autoScroll(interval);
	}

	function autoScroll(interval) {
		setTimeout(function(){
			if (autoAnimate === true) {
				goRight();
				autoScroll(interval);
			}
		},interval);
	}


	function revealGalleryNav() {
		gallery.find('.gallery_nav_wrap').fadeIn(500);
	}


	function bindEvents() {
		
		gallery.on(eventType, '.gallery_nav.left', function(){
			goLeft();
			autoAnimate = false;
		});

		gallery.on(eventType, '.gallery_nav.right', function(e){
			goRight();
			autoAnimate = false;
		});

		indi_bar.on(eventType, '.indicator', function(){
			goTo( $(this).index() );
			autoAnimate = false;
		});

		gallery.on(eventType, '.panel', function(){
			var page_link = $(this).find('.link_to_wine_page').html();
			
			app.selectors_group_1c.addClass('out_1');
			app.selectors_group_2.removeClass('position_2').addClass('position_1 out_1');
			gallery.removeClass('show');
			$('.wine_selection_page_1').removeClass('show');

			setTimeout(function(){
				$( ('.wine_page_' + page_link) ).addClass('show from_filter_page');
			},700);
			app.logging_stats.log( 'pages', $( ('.wine_page_' + page_link) ), '' );

			var wine_name_for_event_tracking = $( ('.wine_page_' + page_link) ).attr('data-stats');
			_gaq.push(['_trackEvent', 'Matt\'s Selector', 'Wine Selection', wine_name_for_event_tracking]);
		});

		$('.append_content').on(eventType, '.wine_page.from_filter_page .back_from_wine_page', function() {
			$(this).parent().removeClass('show from_filter_page');
			setTimeout(function(){
				app.selectors_group_1c.removeClass('out_1');
				app.selectors_group_2.removeClass('position_1 out_1').addClass('position_2');
				gallery.addClass('show');
				$('.wine_selection_page_1').addClass('show');
			},400);
		});


		 

	}


	function calculatePages() {
		var nowVisible = everyItem.filter(":visible");
		nowVisibleLength = nowVisible.length;

		pages = Math.ceil( ((( nowVisibleLength / 3 ) * 10) / 10) );
	}


	function setupItemIndicator() {
		var indicatorHTML = '';
		for ( i=0; i<pages; i++) {
			if (i === 0 ) {
				indicatorHTML += '<div class="indicator sprite current"></div>';
			} else {
				indicatorHTML += '<div class="indicator sprite"></div>';
			}
		}
		indi_bar.html( indicatorHTML );

		// hide the indicator all together if only 1 or less pages
		if (pages < 2) {
			indi_bar.hide();
		} else {
			indi_bar.show();
		}
	}

	
	function centerIfOnly2orLess() {
		if (pages < 2) {
			if ( nowVisibleLength === 2) {
				slideContent.css({
					'-webkit-transform' : 'rotate(0deg) translate(' + 139 + 'px, 0px) scale(1) translateZ(0px)'
				});
			}
			if ( nowVisibleLength === 1) {
				slideContent.css({
					'-webkit-transform' : 'rotate(0deg) translate(' + 277 + 'px, 0px) scale(1) translateZ(0px)'
				});
			}
		} else {
			goTo(0);
		}
	}



	function goRight() {
		if ( currentLocation < pages ) {
			currentLocation = currentLocation + 1;
			goTo(currentLocation);
		}
	}



	function goLeft() {
		if ( currentLocation > 0 ) {
			currentLocation = currentLocation - 1;
			goTo(currentLocation);
		}
	}



	function goTo(nextOne) {
		var leftAdjust = (nextOne * 834);
		slideContent.css({
			'-webkit-transform' : 'rotate(0deg) translate(-' + leftAdjust + 'px, 0px) scale(1) translateZ(0px)'
		});
		var indicators = indi_bar.find('.indicator');
		indicators.removeClass('current');
		indicators.eq(nextOne).addClass('current');
		currentLocation = nextOne;
		calculateDirectionButtons();
	}


	
	_self = {
		init : init,
		update : update
	};
	return _self;
})();












// ADD CUSTOM SELECT STYLING TO JQUERY
// THIS IS MODIFIED TO AVOID USING SELECTS ON ANDROID !!!

(function($){
    $.fn.extend({
    customStyle : function(width_px, height_px, extraClass) {
        if(!$.browser.msie || ($.browser.msie&&$.browser.version>8)) {
            return this.each(function() {
                var currentSelected = $(this).find(':selected');
                var textSpan = ('<span class="customStyleSelectBoxInner">' + currentSelected.text() + '</span><div class="select_ui_element"></div>');

                var wrapElement = '<span class="customStyleSelectBox ' + extraClass + '"></span>';
                
              
                $(this).wrap(wrapElement).animate({
                	'position':'absolute',
                	'opacity': 0,
                	'filter' : 'alpha(opacity=0)',
                	'z-index' : 9
                }, 0, function(){
                	$(this).parent().prepend(textSpan);
                });

           		$(this).hide();

         });
        }
    }
    });
})(jQuery);


if (typeof forward !== "undefined"){
  forward.setListener(function(id, resultCode) {
     console.error('HTTP Store and Forward completed with result: '+resultCode);
  });
}





























































// ===========================================================================================
// STATS
// ===========================================================================================



// ----------------------------------------------------- //
// LOGGING STATS
// ----------------------------------------------------- //

app.logging_stats = (function(){
	
	var _self = {};


	function init(){
		
	} // end init


	function logDetail(detail) {
		sendStatsy(detail);

		var GA_detail = detail.replace(/\./g,'/');
		_gaq.push(['_trackPageview', '/' + GA_detail]);
		
		//_gaq.push(['_trackEvent', 'Videos', 'Play', 'Baby\'s First Birthday']);
	}


	function logStats(scope, element_1, element_2) {
		var data_1 = element_1.attr('data-stats');
		var myData = scope + '.' + data_1;
		if ( element_2 !== '' ) {
			myData += '.' + element_2.attr('data-stats');
		} 
		logDetail(myData);
	}

	
	_self = {
		init : init,
		log : logStats
	};
	return _self;
})();
	
// app.logging_stats.log(element);





// STATSY SEND FUNCTION
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
function sendStatsy(stream) {
  var statsy = {"api_key":statsyData.api_key,"expires":statsyData.expires,"signature":statsyData.signature,"stream_prefix":statsyData.stream_prefix,"events":[]};
    statsy.events.push({stream:stream});
    var site_name_filter = site_name;
    if (site_name === 'firstchoicewineselectorv1' ) {
    	site_name_filter = 'BurwoodEast';
    }
    statsy.events.push({stream:('locations.' + site_name_filter + '.' + stream)});
    // site_name is global variable set in head of master page
   jQuery.ajax('http://statsyapp.com/api/v1/multiple_event', {data: JSON.stringify(statsy), dataType: 'json', type: 'POST', contentType: 'application/json'});
}





//GOOGLE ANALYTICS
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
var ga_account_string = 'UA-37499199-2'; //tooronga-live for default
if (site_name === 'firstchoicewineselectorv1' ) {
	ga_account_string = 'UA-37499199-1';
}
if (site_name === 'tooronga-live' ) {
	ga_account_string = 'UA-37499199-2';
}

var _gaq = _gaq || [];
_gaq.push(['_setAccount', ga_account_string]);
_gaq.push(['_setSessionCookieTimeout', 60000]);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();


<!--======================================================
		wine_selector
		wine_selection_page_1
	=======================================================-->

	
	<div data-stats="Wine-Selection-Page" class="wine_selection_page_1 transitionAll">

		<div class="chalk_banner size_2 position_2 sprite transitionAll">
			<div class="rotate_1">
				<span class="heavy">MATT'S</span>
				<span class="light">wine selector</span>
			</div>
		</div>

		<div class="back_from_wine_selection_page_1 blue_button type_1 transitionAll">
			<div class="back_icon sprite"></div>
			<span class="light"></span>
			<span class="heavy">BACK</span>
			<div class="click_region"></div>
		</div>





		<div class="selection_area">

			<div class="section_1 transitionAll">
				<span class="chalk intro_1 transitionAll_half">1. Select</span>

				<div data-stats="Red-Wine-Filter" class="selector_button transitionAll_half sprite choice_red">
					<span class="text">RED</span>
					<div class="close_icon sprite transitionAll"></div>
					<div class="click_region"></div>
				</div>

				<div data-stats="White-Wine-Filter" class="selector_button transitionAll_half sprite choice_white">
					<span class="text">WHITE</span>
					<div class="close_icon sprite transitionAll"></div>
					<div class="click_region"></div>
				</div>

				<div data-stats="Sparkling-Champagne-Wine-Filter" class="selector_button double_width transitionAll_half sprite choice_sparkling_champagne">
					<span class="text">SPARKLING &amp; CHAMPAGNE</span>
					<div class="close_icon sprite transitionAll"></div>
					<div class="click_region"></div>
				</div>
			</div>


			<div class="section_2 transitionAll">
				<span class="chalk intro_2 transitionAll_half">2. Choose a price range</span>


				<div data-stats="Price-Filter-10-20" class="selector_button transitionAll_half sprite choice_10_20">
					<span class="text">$10 - $20</span>
					<div class="close_icon sprite transitionAll"></div>
					<div class="click_region"></div>
				</div>

				<div data-stats="Price-Filter-20-30" class="selector_button transitionAll_half sprite choice_20_30">
					<span class="text">$20 - $30</span>
					<div class="close_icon sprite transitionAll"></div>
					<div class="click_region"></div>
				</div>

				<div data-stats="Price-Filter-30-50" class="selector_button transitionAll_half sprite choice_30_50">
					<span class="text">$30 - $50</span>
					<div class="close_icon sprite transitionAll"></div>
					<div class="click_region"></div>
				</div>

				<div data-stats="Price-Filter-50-100" class="selector_button transitionAll_half sprite choice_50_100">
					<span class="text">$50 - $100</span>
					<div class="close_icon sprite transitionAll"></div>
					<div class="click_region"></div>
				</div>



			</div>


		</div>




		<%= render :navigation => "Wine-Selector-Wines" , :template_name => 'wine_filter_gallery.nav' %>






		<div class="matt position_2 sprite transitionAll"></div>
		
		<div class="meet_matt_button blue_button type_1 transitionAll">
			<span class="light">meet</span>
			<span class="heavy">MATT SKINNER</span>
			<div class="click_region"></div>
		</div>


			
	</div> <!-- end wine_selection_page_1 -->
<!--======================================================
		firstchoice_wine_selector_v2
		best_buy_page
	=======================================================-->

	
	<div class="best_buy_page main">


			<div class="chalk_banner size_1 position_1 out_1 sprite transitionAll">
				<div class="rotate_1">
					<span class="light">weekly</span>
					<span class="heavy">BEST BUYS</span>
				</div>
			</div>

			

			<div class="back_from_best_buy_page blue_button type_1 out_1 transitionAll">
				<div class="back_icon sprite"></div>
				<span class="light"></span>
				<span class="heavy">BACK</span>
				<div class="click_region"></div>
			</div>



			<!-- repeating region gallery -->
			<div data-stats="Main-Gallery-Scrolled" class="gallery_1 out_1">

				
				


				<%= render :navigation => "Feature-Wines" , :template_name => 'feature_wine_gallery.nav' %>
				




				<div class="gallery_nav_wrap">
					<div class="gallery_nav left sprite"></div>
					<div class="gallery_nav right sprite"></div>
					<!-- populate pagination with js -->
					<div class="inicator_area">
						<div class="indicator_bar"></div>
					</div>
				</div>
			</div> <!-- end gallery_1 -->







			<div class="matt position_4 out_1 sprite transitionAll"></div>

			

			<div class="chalk_text position_1 out_1 transitionAll">
				<span class="line_1">Like to drink better</span>
				<span class="line_2">without spending more?</span>
			</div>

			<div class="green_button position_1 out_1 transitionAll">
				<span class="light">sign up</span>
				<span class="heavy">FOR DEALS</span>
				<div class="mail_icon sprite"></div>
			</div>

		


			
	</div> <!-- end main -->

		
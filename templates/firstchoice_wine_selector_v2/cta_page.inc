<!--======================================================
		firstchoice_wine_selector_v2
		cta_page (call to action)
	=======================================================-->

	<!-- These page items do not have a page of their own as they share parts of the original 'home_page' which is now the best_buy_page. -->


	<div class="chalk_banner size_2 position_2 sprite transitionAll">
		<div class="rotate_1">
			<span class="heavy">MATT'S</span>
			<span class="light">wine selector</span>
		</div>
	</div>

	

	<div data-stats="Launch-Button" class="launch_best_buy_page blue_button type_1 transitionAll">
		<span class="light">weekly</span>
		<span class="heavy">BEST BUYS</span>
		<div class="bottle_icon sprite"></div>
		<div class="click_region"></div>
	</div>



	<!-- large pointer to matts wine selector -->
	<div class="pointer_item">
		<div class="background_note">
			<div class="device_cant_handle_keyframe_timing">
				<div class="wrap_notes">
					<span class="note_1">Need a hand?</span>
					<span class="note_2">Tap the<br/>button below</span>
				</div>
			</div>
		</div>
		<div class="animation_background"></div>
	</div>





	<div class="bottle_footer transitionAll"></div>

	<div class="matt position_5 transitionAll"></div>

	<div class="click_area_for_wine_selector"></div>

	<div class="main_wine_selector_button position_1 transitionAll">
		<span class="light">wine</span>
		<span class="heavy">SELECTOR</span>
		<div class="glasses_icon sprite"></div>
	</div>

		


			


		
<!--======================================================
		firstchoice_wine_selector_v2
		meet_matt_page
	=======================================================-->


	<div data-stats="Meet-Matt-Page" class="meet_matt_page">

			<div class="meet_matt_page_fade"></div>
			<div class="meet_matt_page_white"></div>

			<div class="back_from_meet_matt_page blue_button type_1 transitionAll">
				<div class="back_icon sprite"></div>
				<span class="light"></span>
				<span class="heavy">BACK</span>
				<div class="click_region"></div>
			</div>

			
			<div class="chalk_banner size_3 position_1 sprite transitionAll">
				<div class="rotate_1">
					<span class="light">meet</span>
					<span class="heavy">MATT SKINNER</span>
				</div>
			</div>

			<div class="about_matt_paragraph_section first">
				<span class="heading">Award winning author</span>
				<span class="copy">of &#8220;Thirsty Work&#8221; and &#8220;Heard it through the Grapevine&#8221;</span>
			</div>

			<div class="about_matt_paragraph_section">
				<span class="heading">Wine advisor</span>
				<span class="copy">to Jamie Oliver's Fifteen Restaurant Group</span>
			</div>

			<div class="about_matt_paragraph_section">
				<span class="heading">Colomnist</span>
				<span class="copy">for &#8220;Gourmet Traveller WINE&#8221;</span>
			</div>

			<div class="about_matt_paragraph_section">
				<span class="heading">Background</span>
				<span class="copy">Matt Skinner has been tasting, talking and writing about wine for almost 20 years. And now he&#8217;s excited to join the First Choice Liquor team.</span>
			</div>


			<div class="matt_quote">
				<span class="heading">"I love what I do. It's not just a job for me; it's a passion. And it is for the whole team at First Choice too."</span>
				<div class="matt_signature sprite"></div>
			</div>


			<div class="matt position_3 sprite transitionAll"></div>


	</div>
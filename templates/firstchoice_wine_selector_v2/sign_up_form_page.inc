<!--======================================================
		firstchoice_wine_selector_v2
		sign_up_form_page
	=======================================================-->


	<div data-stats="Sign-Up-Page" class="sign_up_form_page">
			<div class="signup_page_fade"></div>

			<div class="chalk_banner size_4 position_1 sprite">
				<div class="rotate_1">
					<span class="light"><%= render :textual => 'chalk_banner_light_2', :default => 'sign me up', :maxlength => '10' %></span>
					<span class="heavy"><%= render :textual => 'chalk_banner_heavy_2', :default => 'FOR DEALS', :maxlength => '10' %></span>
				</div>
			</div>

			<div class="form_div">
				<div class="close_form sprite"></div>
				<!-- <div class="next_field sprite"></div> -->
				<div class="close_keyboard sprite"></div>




				<form data-stats="submit" method="POST" action="" id="standard_form" class="standard_form clearfix" autocomplete="off">

					<div class="form_action" style="display:none;"><%= form_action_url('deal signup') %>.json</div>


					<div class="input first full">
						<label for="first_name">Name <abbr title="required">*</abbr></label>
						<input id="first_name" type="text" value="" class="required" name="first_name" autocomplete="off" placeholder="Tap here to enter your details...">
					</div>


					<div class="clearfix"></div>


					<div class="input second full">
						<label for="email">Email <abbr title="required">*</abbr></label>
						<!-- <input id="email_part_1" type="text" value="" class="required" name="email_part_1" autocomplete="off"> -->
						<!-- <span class="atSymbol">@</span> -->
						<!-- <input id="email_part_2" type="text" value="" class="required" name="email_part_2" autocomplete="off"> -->
						<input id="email" type="email" value="" class="email required" name="email" autocomplete="off">
					</div>
						



					<div class="clearfix"></div>


					<div class="input third quarter">
						<label for="postcode">Postcode <abbr title="required">*</abbr></label>
						<input id="postcode" type="text" value="" class="required" name="postcode" autocomplete="off">	
					</div>



					<div class="clearfix"></div>


					
					<input id="dob" type="hidden" value="" class="" name="dob" autocomplete="off">	
					



					<!-- <div class="input checkbox">
						<div class="custom_checkbox sprite">
							<input type="checkbox" class="custom" value="191" id="mailing_list" name="interests[]"/>
						</div>
						<span class="c">I would like to receive The Ian Potter Foundation Newsletter</span>
					</div>

					<div class="input checkbox">
						<div class="custom_checkbox sprite">
							<input type="checkbox" class="custom" value="235" id="annual_report" name="interests[]"/>
						</div>
						<span class="c">I would like to receive The Ian Potter Foundation Annual Report</span>
					</div> -->


					<input id="submit_enquiry" class="sprite" type="submit" value="submit"/>


				</form>



				




				<div class="date_select_tool">
					<label>D.O.B</label>

					<select id="dob_day" class="custom">
						<option>1</option>
					</select>

					<select id="dob_month" class="custom">
						<option>JAN</option>
					</select>

					<select name="dob_year" id="dob_year" class="custom">
		            	<option value="1990">1990</option>
		            </select>

				</div>






			</div>






			<div id="dayPicker">
				<div class="ui_element">
					<div id="dayPickeriScroll">
						<div id="dayScroller">
							<div class="day"><span>1</span></div>
							<div class="day"><span>2</span></div>
							<div class="day"><span>3</span></div>
							<div class="day"><span>4</span></div>
							<div class="day"><span>5</span></div>
							<div class="day"><span>6</span></div>
							<div class="day"><span>7</span></div>
							<div class="day"><span>8</span></div>
							<div class="day"><span>9</span></div>
							<div class="day"><span>10</span></div>
							<div class="day"><span>11</span></div>
							<div class="day"><span>12</span></div>
							<div class="day"><span>13</span></div>
							<div class="day"><span>14</span></div>
							<div class="day"><span>15</span></div>
							<div class="day"><span>16</span></div>
							<div class="day"><span>17</span></div>
							<div class="day"><span>18</span></div>
							<div class="day"><span>19</span></div>
							<div class="day"><span>20</span></div>
							<div class="day"><span>21</span></div>
							<div class="day"><span>22</span></div>
							<div class="day"><span>23</span></div>
							<div class="day"><span>24</span></div>
							<div class="day"><span>25</span></div>
							<div class="day"><span>26</span></div>
							<div class="day"><span>27</span></div>
							<div class="day"><span>28</span></div>
							<div class="day"><span>29</span></div>
							<div class="day"><span>30</span></div>
							<div class="day"><span>31</span></div>
						</div>
					</div>
					<div class="choices">
						<div class="cancel"><span>Cancel</span></div>
						<div class="set"><span>Set</span></div>
					</div>
				</div>
			</div>


			<div id="monthPicker">
				<div class="ui_element">
					<div id="monthPickeriScroll">
						<div id="monthScroller">
							<div class="month"><span>JAN</span></div>
							<div class="month"><span>FEB</span></div>
							<div class="month"><span>MAR</span></div>
							<div class="month"><span>APR</span></div>
							<div class="month"><span>MAY</span></div>
							<div class="month"><span>JUN</span></div>
							<div class="month"><span>JUL</span></div>
							<div class="month"><span>AUG</span></div>
							<div class="month"><span>SEP</span></div>
							<div class="month"><span>OCT</span></div>
							<div class="month"><span>NOV</span></div>
							<div class="month"><span>DEC</span></div> 
						</div>
					</div>
					<div class="choices">
						<div class="cancel"><span>Cancel</span></div>
						<div class="set"><span>Set</span></div>
					</div>
				</div>
			</div>


			<div id="yearPicker">
				<div class="ui_element">
					<div id="yearPickeriScroll">
						<div id="yearScroller">
							<!-- <div class="year"><span>1990</span></div> -->
							<!-- append in js -->
						</div>
					</div>
					<div class="choices">
						<div class="cancel"><span>Cancel</span></div>
						<div class="set"><span>Set</span></div>
					</div>
				</div>
			</div>



	</div>